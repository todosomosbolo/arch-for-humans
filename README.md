# Arch for humans

Only a little bash script for installing the base Arch Linux and the DistroTube version of:
- Dwm (Dynamic Window Manager by Suckless Team)
- Dwmblocks (An third-party program for easy configuration of dwm's panel)
- St (Simple Terminal by Suckless Team)
