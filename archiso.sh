#!/usr/bin/env bash

ls /usr/share/kbd/keymaps/**/*.map.gz
loadkeys br-abnt2
ls /usr/share/kbd/consolefonts/
setfont ter-132n

echo "pt_BR.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
export LANG=pt_BR.UTF-8

ls /sys/firmware/efi/efivars

ip link
ping google.com

timedatectl set-ntp true
timedatectl status

fdisk -l
fdisk /dev/sda

mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
mkswap /dev/sda4

mount /dev/sda2 /mnt
mkdir /mnt/{boot,home}
mount /dev/sda1 /mnt/boot
mount /dev/sda3 /mnt/home
swapon /dev/sda4

pacstrap /mnt base linux linux-firmware linux-headers base-devel networkmanager wpa_supplicant bluez bluez-utils dialog nano man-db man-pages texinfo git bash-completion terminus-font

genfstab -Up /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab

cp -r ~/arch-for-humans /mnt
arch-chroot /mnt
poweroff
