#!/usr/bin/env bash

nmtui && sudo pacman -Syyu

sudo pacman -S xorg xterm lightdm lightdm-webkit2-greeter nitrogen lxappearance picom nemo cinnamon-translations
systemctl enable lightdm
nano /etc/lightdm/lightdm.conf

git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin && makepkg -si

yay -S dwm-distrotube-git dwmblocks-distrotube-git dmenu-distrotube-git st-distrotube-git shell-color-scripts
yay -S exa && nano ~/.bashrc

sudo reboot
